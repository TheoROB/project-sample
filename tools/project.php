<?php


function get_project($id){
    global $cnx;
    $sth = $cnx->prepare("SELECT * FROM projects WHERE ID = :id;");
    $sth->execute(["id"=>$id]);
    return $sth->fetchAll();
}



function get_all_projects(){
    global $cnx;
    $sth = $cnx->prepare("SELECT * FROM projects;");
    $sth->execute();
    return $sth->fetchAll();
}



function add_project($name, $detail){
    $err = [];
    if(empty($_GET['name'])){
        array_push($err, "You have to add a name of project");
    }
    if(empty($_GET['detail'])){
        array_push($err, "You have to add a details of project");
    }
    if(!empty($err)){
        foreach($err as $er){
           ?>
          <div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Ups!</strong> <?= $er ?>.
</div>
           <?php
        }
    }else{
        global $cnx;
        $sth = $cnx->prepare("INSERT INTO projects (name, detail) VALUES (:name, :detail);");
        $sth->execute([":name"=>$name, ":detail"=>$detail]);
        return $cnx->lastInsertId();
    }
   
}



function delete_project($id){
    global $cnx;
    $sth = $cnx->prepare("DELETE FROM projects WHERE id = :idproject;");
    $foo = $cnx->prepare("DELETE FROM participations WHERE FK_project = :idproject;");
    $foo->execute(["idproject"=>$id]);
    return $sth->execute(["idproject"=>$id]);
}



function update_project($id, $name, $detail){
    global $cnx;
    $sth = $cnx->prepare("UPDATE projects SET name = :name, detail = :detail WHERE id = :id;");
    return $sth->execute(["id"=>$id, "name"=>$name, "detail"=>$detail]);
}