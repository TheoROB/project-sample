<?php




function get_project_user($idproject){
    global $cnx;
    $sth = $cnx->prepare("
        SELECT * 
        FROM participations
        JOIN users
            ON participations.FK_user = users.ID
        WHERE FK_project = :idproject
    ");
    $sth->execute(["idproject"=>$idproject]);
    return $sth->fetchAll();
}



function get_user($id){
    global $cnx;
    $sth = $cnx->prepare("SELECT * FROM users WHERE ID = :id;");
    $sth->execute(["id"=>$id]);
    return $sth->fetchAll();
}



function get_all_users(){
    global $cnx;
    $sth = $cnx->prepare("SELECT * FROM users;");
    $sth->execute();
    return $sth->fetchAll();
}



function add_user($firstname, $lastname, $role){
    global $cnx;
    $sth = $cnx->prepare("INSERT INTO users (firstname, lastname, role) VALUES (:firstname, :lastname, :role);");
    $sth->execute(["firstname"=>$firstname, "lastname"=>$lastname, "role"=>$role]);
    return $cnx->lastInsertId();
}



function delete_user($id){
    // TODO
    return true;
}



function update_user($id, $name){
    // TODO
    return true;
}



function print_user( $user ){
    
    switch($user['role']){
        case "développeur": 
            echo ' <span class="glyphicon glyphicon-wrench"></span>';
            break;

        case "client": 
            echo ' <span class="glyphicon glyphicon-euro"></span>';
            break;

        default:
            echo ' <span class="glyphicon glyphicon-question-sign"></span>';
    }

    echo " " . $user['firstname'] . " " . $user['lastname'] . " ";
}

