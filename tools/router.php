<?php

$message = "";

switch($_GET['action']){
    case "addproject"; 
    $message = add_project($_REQUEST['name'], $_REQUEST['detail'])
        ? "Projet ajouté avec succès" 
        : "L'ajout du projet a échoué :(";
    break;

    case "changeproject"; 
    $message = update_project($_REQUEST['ID'], $_REQUEST['name'], $_REQUEST['detail'])
        ? "Projet mis à jour avec succès !" 
        : "La modification du projet a échoué :(";
    break;

    case "deleteproject"; 
    $message = delete_project($_REQUEST['ID'])
        ? "Suppression réalisée avec succès !" 
        : "La suppression du projet a échoué :(";
    break;

    case "addparticipation"; 
    $message = add_participation($_REQUEST['idproject'], $_REQUEST['iduser'])
        ? "Participation acceptée avec succès !" 
        : "Action échouée : sélectionnez un utilisateur qui ne participe pas déjà à ce projet !";
    break;

    case "deleteparticipation"; 
    $message = delete_participation($_REQUEST['idproject'], $_REQUEST['iduser'])
        ? "Personne retirée de ce projet !" 
        : "Le retrait de cette personne a échoué :(";
    break;

    case "adduser"; 
    $message = add_user($_REQUEST['firstname'], $_REQUEST['lastname'], $_REQUEST['role'])
        ? "Personne ajoutée avec succès" 
        : "Ajout de la personné échoué :(";

    break;

}