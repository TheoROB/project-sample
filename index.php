<?php

require('configuration.php');
require('tools/DB.php');
require('tools/project.php');
require('tools/user.php');
require('tools/participation.php');
require('tools/router.php');



// Request all users once, as it will be used many times...

$all_users = get_all_users();

$all_projects = get_all_projects();


?><!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Gestion de projet</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
  </head>
  <body>

    <div class="container">


        <?php if(count($all_projects)): ?>


        <h2>Liste des projets</h2>

        <table class="table table-striped table-bordered table-hover table-condensed">
            <thead>
                <tr>
                    <th class="col-md-6">Projet (nom, détail)</th>
                    <th class="col-md-3">Participants</th>
                    <th class="col-md-3"></th>
                </tr>
            </thead>


            <tbody>
                <?php foreach($all_projects as $project): ?>
                <tr>
                    <td class="project">
                        <form action="." class="form-inline">
                        <input type="text" name="name" value="<?=$project['name']?>" class="form-control" />
                        <input type="text" name="detail" value="<?=$project['detail']?>" class="form-control" />
                            <input type="hidden" name="ID" value="<?=$project['ID']?>" />
                            <input type="hidden" name="action" value="changeproject" />
                            <button class="btn btn-warning"><span class="glyphicon glyphicon-refresh"></span></button>
                        </form>

                        <form action=".">
                            <input type="hidden" name="ID" value="<?=$project['ID']?>" />
                            <input type="hidden" name="action" value="deleteproject" />
                            <button class="btn btn-danger" onclick="return confirm('tes sur ?')" ><span class="glyphicon glyphicon-remove"></span></button>
                        </form>
                    </td>
                    <td class="participant">
                        <?php foreach(get_project_user($project['ID']) as $user): ?>
                            
                            <form action=".">
                                <?php print_user( $user ); ?>
                                <input type="hidden" name="iduser" value="<?=$user['ID']?>" />
                                <input type="hidden" name="idproject" value="<?=$project['ID']?>" />
                                <input type="hidden" name="action" value="deleteparticipation" />
                                <button class="btn btn-danger pull-right" onclick="return confirm('tes sur ?')"><span class="glyphicon glyphicon-remove"></span></button>
                            </form>
                            <br />

                        <?php endforeach; ?>
                    </td>
                    <td>
                        <form action="." class="form-inline">
                            <input type="hidden" name="action" value="addparticipation" />
                            <input type="hidden" name="idproject" value="<?=$project['ID']?>" />
                            <select name="iduser" class="form-control">
                                <option selected disabled>ajouter une personne</option>
                                <?php foreach($all_users as $p): ?>
                                    <option value="<?=$p['ID']?>">
                                        <?php print_user( $p );?>
                                    </option>
                                <?php endforeach ?>
                            </select>
                            <button class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button>
                        </form>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <?php else:
        
            $message = "Veuiller créer un projet pour commencer !";

        endif; ?>




        <?php if($message): ?>
            <p class="alert <?= strpos($message,'échoué')?'alert-danger':'alert-success'?>">
                <?= $message ?>
            </p>
        <?php endif; ?>



        <div class="row">

            <div class="col-md-6">
                <h2>Ajouter un projet</h2>
                <form action=".">
                    <input type="hidden" name="action" value="addproject" />

                    <div class="form-group">
                        <label for="name">Nom du projet</label>
                        <input type="text" name="name" value="" class="form-control" placeholder="le nom du projet" />
                    </div>

                    <div class="form-group">
                        <label for="detail">Détail</label>
                        <textarea name="detail" class="form-control" placeholder="une petite description"></textarea>
                    </div>

                    <button class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button>
                </form>
            </div>

            <div class="col-md-6">
                <h2>Ajouter une personne</h2>
                <form action=".">

                    <input type="hidden" name="action" value="adduser" />

                    <div class="form-group">
                        <label for="firstname">Prénom</label>
                        <input type="text" name="firstname" value="" class="form-control" placeholder="prénom" />
                    </div>

                    <div class="form-group">
                        <label for="lastname">Nom</label>
                        <input type="text" name="lastname" value="" class="form-control" placeholder="nom" />
                    </div>

                    <div class="form-group">
                        <label for="role">Rôle</label>
                        <select name="role" class="form-control">
                            <option selected disabled>attribuer un rôle</option>
                            <option>développeur</option>
                            <option>client</option>
                        </select>
                    </div>



                    <div class="form-group">
                        <label for="level">Expérience</label>
                        <select name="level" class="form-control">
                            <option selected disabled>affecter une expérience</option>
                            <option>junior</option>
                            <option>intermédiaire</option>
                            <option>sénior</option>
                        </select>
                    </div>



                    <div class="form-group">
                        <label for="address">Rue</label>
                        <input type="text" name="address" value="" class="form-control" placeholder="rue" />
                    </div>

                    <div class="form-group">
                        <label for="postcode">Code postal</label>
                        <input type="text" name="postcode" value="" class="form-control" placeholder="code postal" />
                    </div>

                    <div class="form-group">
                        <label for="city">Ville</label>
                        <input type="text" name="city" value="" class="form-control" placeholder="ville" />
                    </div>

                    <button class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button>
                </form>
            </div>

        </div>

    </div>


    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
  </body>
</html>
